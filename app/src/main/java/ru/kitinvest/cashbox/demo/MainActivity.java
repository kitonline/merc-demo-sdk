package ru.kitinvest.cashbox.demo;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.kitinvest.cashbox.sdk.CashboxCoreService;
import ru.kitinvest.cashbox.sdk.domain.CheckData;
import ru.kitinvest.cashbox.sdk.domain.CheckItem;
import ru.kitinvest.cashbox.sdk.domain.CorrectionInfo;
import ru.kitinvest.cashbox.sdk.domain.CorrectionMoney;
import ru.kitinvest.cashbox.sdk.domain.DeviceInfo;
import ru.kitinvest.cashbox.sdk.domain.FiscalData;
import ru.kitinvest.cashbox.sdk.domain.PrintFrag;
import ru.kitinvest.cashbox.sdk.domain.RegisterData;
import ru.kitinvest.cashbox.sdk.domain.enums.AgentType;
import ru.kitinvest.cashbox.sdk.domain.enums.CalculationMethod;
import ru.kitinvest.cashbox.sdk.domain.enums.CalculationType;
import ru.kitinvest.cashbox.sdk.domain.enums.CorrectionType;
import ru.kitinvest.cashbox.sdk.domain.enums.FontDecoration;
import ru.kitinvest.cashbox.sdk.domain.enums.FontSize;
import ru.kitinvest.cashbox.sdk.domain.enums.NdsRate;
import ru.kitinvest.cashbox.sdk.domain.enums.ProductAttribute;
import ru.kitinvest.cashbox.sdk.domain.enums.RegReason_1_1;
import ru.kitinvest.cashbox.sdk.domain.enums.TaxMode;
import ru.kitinvest.cashbox.sdk.domain.enums.TextAlign;
import ru.kitinvest.cashbox.sdk.domain.enums.WorkMode;
import ru.kitinvest.cashbox.sdk.exception.CommandExecutionException;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    private CashboxCoreService coreService;

    private ProgressDialog progressDialog;

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Executors.newCachedThreadPool().submit(() -> {
                CashboxCoreService.LocalBinder binder = (CashboxCoreService.LocalBinder) service;
                coreService = binder.getService();

                try {
                     /*
                    проводим первичную инциализацию
                    метод блокирующий и возвращает управление после того как пройдена инциализация
                    */
                    boolean result = coreService.init();

                     /*
                    следом можем почитать информацию о регистрационных данных
                    */
                    RegisterData regDataJson = coreService.getRegisterData();
                    Timber.i(new Gson().toJson(regDataJson));

                     /*
                    данные об устройстве
                    */
                    DeviceInfo deviceInfoJson = coreService.getDeviceInfo();
                    Timber.i(new Gson().toJson(deviceInfoJson));

                } catch (CommandExecutionException | RemoteException e) {
                    Timber.w(e);
                } finally {
                    runOnUiThread(() -> progressDialog.dismiss());
                }

            });


        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            coreService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Timber.plant(new Timber.DebugTree());

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        progressDialog.show();
        bindService(new Intent(this, CashboxCoreService.class), serviceConnection, Context.BIND_AUTO_CREATE); //биндимя к сервису
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (coreService != null)
            unbindService(serviceConnection);
    }

    @OnClick(R.id.resetFn)
    void resetFn() {
        progressDialog.show();
        Executors.newCachedThreadPool().submit(() -> {
            try {
                coreService.resetFn();
            } catch (CommandExecutionException | RemoteException e) {
                Timber.w(e);
            }
            runOnUiThread(() -> progressDialog.dismiss());
        });
    }

    @OnClick(R.id.registerFn)
    void registerFn() {
        progressDialog.show();
        Executors.newCachedThreadPool().submit(() -> {
            try {
                RegisterData registerData = new RegisterData();

                long reason_1_1 = RegReason_1_1.RR_2.getId() | RegReason_1_1.RR_7.getId() | RegReason_1_1.RR_3.getId();
                registerData.setReasons_1_1(reason_1_1);

                registerData.setOrgName("ООО \"КИТ ИНВЕСТ\"");
                registerData.setOrgAddress("248032, г. Калуга, ул. Мичурина, д.11");
                registerData.setOrgPlace("офис 22");
                registerData.setOrgInn("4004007928");
                registerData.setRnm("0000000005032204");
                registerData.setUserName("Пушкин А.С.");
                registerData.setOfdName("ОАО \"ЭСК\"");
                registerData.setOfdInn("7751021489");
                registerData.setSenderEmail("sender@gmail.com");
                registerData.setAutomat("55010100243"); //Номер ККМ

                registerData.setWorkMode(WorkMode.Encrypt.getId() | WorkMode.Automatical.getId()); //битовая маска редимов работы ККТ ()
                registerData.setAgent(AgentType.BPSA.getId() | AgentType.COMISSIONER.getId() | AgentType.TRUSTED.getId()); //битовая маска - признак платежного агента, 0 - НЕАГЕНТ
                registerData.setOrgTaxSystem(TaxMode.COMMON.getId() | TaxMode.USN_PLUS.getId()); //битовая маска систем налогообложения

                coreService.processRegister(registerData);
            } catch (CommandExecutionException | RemoteException e) {
                Timber.w(e);
            }
            runOnUiThread(() -> progressDialog.dismiss());
        });
    }

    @OnClick(R.id.openSession)
    void openSession() {
        progressDialog.show();
        Executors.newCachedThreadPool().submit(() -> {
            try {
                FiscalData fiscalData = coreService.processOpenSession("Кассир 1", "1234567890");
                Timber.i(new Gson().toJson(fiscalData));
            } catch (CommandExecutionException | RemoteException e) {
                Timber.w(e);
            }
            runOnUiThread(() -> progressDialog.dismiss());
        });
    }

    @OnClick(R.id.closeSession)
    void closeSession() {
        progressDialog.show();
        Executors.newCachedThreadPool().submit(() -> {
            try {
                FiscalData fiscalData = coreService.processCloseSession("Кассир 1", "1234567890");
                Timber.i(new Gson().toJson(fiscalData));
            } catch (CommandExecutionException | RemoteException e) {
                Timber.w(e);
            }
            runOnUiThread(() -> progressDialog.dismiss());
        });
    }

    @OnClick(R.id.check)
    void check() {
        progressDialog.show();
        CheckData checkData = new CheckData();
        checkData.setCashier("Покупатель 1");
        checkData.setTaxSystem(TaxMode.USN_PLUS.getId());
        checkData.setCalculationType(CalculationType.INCOMING.getId());
        checkData.setPayCash(500000);//в копейках



        CheckItem product = new CheckItem(
                "Ставка азартной игры",
                "",
                1258.58,
                1d,
                NdsRate.NDS_20_TYPE.getId(),
                ProductAttribute.ATTR_5.getId(),
                CalculationMethod.M_4.getId(),
                null,
                false,
                0.0,
                "",
                "",
                "",
                "");

        Executors.newCachedThreadPool().submit(() -> {
            try {
                FiscalData fiscalData = coreService.processPrintCheck(checkData, Collections.singletonList(product));
                Timber.i(new Gson().toJson(fiscalData));
            } catch (CommandExecutionException | RemoteException e) {
                Timber.w(e);
            }
            runOnUiThread(() -> progressDialog.dismiss());
        });
    }

    @OnClick(R.id.correction)
    void correction() {
        progressDialog.show();
        CheckData checkData = new CheckData();
        checkData.setCashier("Покупатель 1");
        checkData.setTaxSystem(TaxMode.COMMON.getId());
        checkData.setCalculationType(CalculationType.INCOMING.getId());
        checkData.setPayCash(500000);//в копейках

        CheckItem product = new CheckItem(
                "Позиция 1",
                "",
                1258.58,
                1d,
                NdsRate.NDS_20_TYPE.getId(),
                ProductAttribute.ATTR_5.getId(),
                CalculationMethod.M_4.getId(),
                null,
                false,
                0.0,
                "",
                "",
                "",
                "");

        CorrectionInfo correctionInfo = new CorrectionInfo(
                CorrectionType.SELF.getId(), //тип коррекции
                "Описание", //Описание коррекции
                System.currentTimeMillis() / 1000, //дата док-та основания для коррекции (в формате UNIXTIME - кол-во секунд начиная с 01.01.1970
                "123",  //номер документиа основания для коррекции
                null
        );

/*
        List<PrintFrag> fragments = new ArrayList<PrintFrag>();
        fragments.add(new PrintFrag("Hello world", TextAlign.LEFT.Id(), FontSize.SMALL.getSize(), FontDecoration.BOLD.getId(), false));
        fragments.add(new PrintFrag("Номер транзакции  1234", TextAlign.CENTER.Id(), FontSize.MEDIUM.getSize(), FontDecoration.NONE.getId(), false));
        fragments.add(new PrintFrag("Доп.Номер = xx012", TextAlign.RIGHT.Id(), FontSize.LARGE.getSize(), FontDecoration.ITALIC.getId(), false));
        fragments.add(new PrintFrag("Hello world", TextAlign.RIGHT.Id(), FontSize.LARGE.getSize(), FontDecoration.ITALIC.getId(), false));
        checkData.setFragments(fragments);
*/
        Executors.newCachedThreadPool().submit(() -> {
            try {
                FiscalData fiscalData = coreService.processPrintCheckCorrection(checkData, Collections.singletonList(product), correctionInfo);
                Timber.i(new Gson().toJson(fiscalData));
            } catch (CommandExecutionException | RemoteException e) {
                Timber.w(e);
            }
            runOnUiThread(() -> progressDialog.dismiss());
        });
    }

    @OnClick(R.id.correction105)
    void correction105() {

        CorrectionInfo correctionInfo = new CorrectionInfo(
                CorrectionType.SELF.getId(), //тип коррекции
                "Описание", //Описание коррекции
                System.currentTimeMillis() / 1000, //дата док-та основания для коррекции (в формате UNIXTIME - кол-во секунд начиная с 01.01.1970
                "123", //номер документиа основания для коррекции
                null
        );

        List<PrintFrag> fragments = new ArrayList<PrintFrag>();
        fragments.add(new PrintFrag("Hello world", TextAlign.LEFT.Id(), FontSize.SMALL.getSize(), FontDecoration.BOLD.getId(), false));
        fragments.add(new PrintFrag("Номер транзакции  1234", TextAlign.CENTER.Id(), FontSize.MEDIUM.getSize(), FontDecoration.NONE.getId(), false));
        fragments.add(new PrintFrag("Доп.Номер = xx012", TextAlign.RIGHT.Id(), FontSize.LARGE.getSize(), FontDecoration.ITALIC.getId(), false));
        fragments.add(new PrintFrag("Hello world", TextAlign.RIGHT.Id(), FontSize.LARGE.getSize(), FontDecoration.ITALIC.getId(), false));

        correctionInfo.setFragments(fragments);


        CorrectionMoney correctionMoney = new CorrectionMoney(
                1000L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 1000L, 0L, 0L
        );

        Executors.newCachedThreadPool().submit(() -> {
            try {
                FiscalData fiscalData = coreService.processCorrection("Кассир 1", "1234567890", TaxMode.ENVD.getId(), CalculationType.INCOMING.getId(), correctionInfo, correctionMoney);
                Timber.i(new Gson().toJson(fiscalData));
            } catch (CommandExecutionException | RemoteException e) {
                Timber.w(e);
            }
            runOnUiThread(() -> progressDialog.dismiss());
        });
    }

    @OnClick(R.id.check_with_fragments)
    void check_with_fragments() {
        progressDialog.show();
        CheckData checkData = new CheckData();
        checkData.setCashier("Иванов Иван Петрович");
        checkData.setTaxSystem(TaxMode.ENVD.getId());
        checkData.setCalculationType(CalculationType.INCOMING.getId());
        checkData.setPayCash(500000);//в копейках

        List<PrintFrag> fragments = new ArrayList<PrintFrag>();
        fragments.add(new PrintFrag("Hello world", TextAlign.LEFT.Id(), FontSize.SMALL.getSize(), FontDecoration.BOLD.getId(), false));
        fragments.add(new PrintFrag("Номер транзакции  1234", TextAlign.CENTER.Id(), FontSize.MEDIUM.getSize(), FontDecoration.NONE.getId(), false));
        fragments.add(new PrintFrag("Доп.Номер = xx012", TextAlign.RIGHT.Id(), FontSize.LARGE.getSize(), FontDecoration.ITALIC.getId(), false));
        fragments.add(new PrintFrag("Hello world", TextAlign.RIGHT.Id(), FontSize.LARGE.getSize(), FontDecoration.ITALIC.getId(), false));

        //align;  //LEFT(1, "left"),CENTER(2, "center"),RIGHT(3, "right");
        //size; //SMALLER(-1),SMALL(0),MEDIUM(1),LARGE(2);
        //decoration; //    NONE(0),BOLD(1),ITALIC(2);

        checkData.setFragments(fragments);


        CheckItem product = new CheckItem(
                "Батон нарезной",
                "",
                1258.58,
                1d,
                NdsRate.NDS_20_TYPE.getId(),
                ProductAttribute.ATTR_5.getId(),
                CalculationMethod.M_4.getId(),
                null,
                false,
                0.0,
                "",
                "",
                "",
                "");

        Executors.newCachedThreadPool().submit(() -> {
            try {
                FiscalData fiscalData = coreService.processPrintCheck(checkData, Collections.singletonList(product));
                Timber.i(new Gson().toJson(fiscalData));
            } catch (CommandExecutionException | RemoteException e) {
                Timber.w(e);
            }
            runOnUiThread(() -> progressDialog.dismiss());
        });
    }

    @OnClick(R.id.fragments_only)
    void fragments_only() {
        progressDialog.show();

        List<PrintFrag> fragments = new ArrayList<PrintFrag>();
        fragments.add(new PrintFrag("Hello world", TextAlign.LEFT.Id(), FontSize.SMALL.getSize(), FontDecoration.BOLD.getId(), false));
        fragments.add(new PrintFrag("Номер транзакции  1234", TextAlign.CENTER.Id(), FontSize.MEDIUM.getSize(), FontDecoration.NONE.getId(), false));
        fragments.add(new PrintFrag("Доп.Номер = xx012", TextAlign.RIGHT.Id(), FontSize.LARGE.getSize(), FontDecoration.ITALIC.getId(), false));
        fragments.add(new PrintFrag("Hello world", TextAlign.RIGHT.Id(), FontSize.LARGE.getSize(), FontDecoration.ITALIC.getId(), false));

        //align;  //LEFT(1, "left"),CENTER(2, "center"),RIGHT(3, "right");
        //size; //SMALLER(-1),SMALL(0),MEDIUM(1),LARGE(2);
        //decoration; //    NONE(0),BOLD(1),ITALIC(2);


        Executors.newCachedThreadPool().submit(() -> {
            try {
                coreService.printFragments(fragments);
            } catch (CommandExecutionException | RemoteException e) {
                Timber.w(e);
            }
            runOnUiThread(() -> progressDialog.dismiss());
        });
    }
}
